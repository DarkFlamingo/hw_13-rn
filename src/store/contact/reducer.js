import { createReducer } from '@reduxjs/toolkit';
import { loadContact, addContact, deleteContact } from './actions';

const initialState = {
  contacts: [],
};

const reducer = createReducer(initialState, (builder) => {
  builder.addCase(loadContact, (state, action) => {
    const { contacts } = action.payload;

    state.contacts = contacts;
  });
  builder.addCase(addContact, (state, action) => {
    const { contact } = action.payload;

    state.contacts = [contact, ...state.contacts];
  });
  builder.addCase(deleteContact, (state, action) => {
    const { id } = action.payload;

    state.contacts = state.contacts.filter((contact) => contact.id !== id);
  });
});

export { reducer };
