import { createAction } from '@reduxjs/toolkit';

const ActionType = {
  LOAD_CONTACT: 'contact/load-contact',
  ADD_CONTACT: 'contact/add-contact',
  DELETE_CONTACT: 'contact/delete-contact',
};

const loadContact = createAction(ActionType.LOAD_CONTACT, (contacts) => ({
  payload: {
    contacts,
  },
}));

const addContact = createAction(ActionType.ADD_CONTACT, (contact) => ({
  payload: {
    contact,
  },
}));

const deleteContact = createAction(ActionType.DELETE_CONTACT, (id) => ({
  payload: {
    id,
  },
}));

export { loadContact, addContact, deleteContact };
