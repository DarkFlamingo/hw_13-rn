import { reducer as contactReducer } from './contact/reducer';
import { combineReducers } from '@reduxjs/toolkit';

const rootReducer = combineReducers({
  contact: contactReducer,
});

export { rootReducer };
