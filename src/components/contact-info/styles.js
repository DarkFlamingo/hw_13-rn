import { StyleSheet } from 'react-native';

export const styles = StyleSheet.create({
  contactInfoWrapper: {
    flex: 1,
  },
  centerWrp: {
    flex: 1,
    alignItems: 'center',
    marginTop: 100
  },
  photoWrapper: {
    alignItems: 'center',
  },
  textWrapper: { 
    marginLeft: 50,
    marginRight: 50,
   },
  buttonWrapper: {
    flexDirection: 'row',
  },
  image: {
    marginVertical: 30,
    width: 300,
    height: 300,
    borderRadius: 100,
  },
  textLabel: {
    marginTop: 20,
    color: '#767876'
  },
  mainText: {
    fontSize: 25,
  },
  phoneLabel: {},
  phoneText: {
    fontSize: 25,
  },
  callBtn: {
    margin: 10,
    marginRight: 30,
    backgroundColor: '#7BEC7E',
    justifyContent: 'center',
    paddingHorizontal: 20,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
    borderRadius: 30,
    paddingHorizontal: 30,
    paddingVertical: 10,
  },
  deleteBtn: {
    margin: 10,
    marginLeft: 30,
    backgroundColor: '#F65454',
    justifyContent: 'center',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
    borderRadius: 30,
    paddingHorizontal: 30,
    paddingVertical: 10,
  },
  deleteBtnSingle: {
    margin: 10,
    backgroundColor: '#F65454',
    justifyContent: 'center',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
    borderRadius: 30,
    paddingHorizontal: 30,
    paddingVertical: 10,
  },
  deleteBtnText: {
    fontSize: 24,
    color: '#2F2F2F'
  },
});
