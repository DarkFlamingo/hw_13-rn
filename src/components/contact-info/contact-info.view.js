import React from 'react';
import { View, Text, Image, Button, TouchableOpacity } from 'react-native';
import { styles } from './styles';
import Communications from 'react-native-communications';
import { useDispatch } from 'react-redux';
import { contactActionCreator } from '../../store/actions';

const ContactInfo = ({ route, navigation }) => {
  const { item } = route.params;

  const dispatch = useDispatch();

  const handleContactDelete = React.useCallback(
    (id) => dispatch(contactActionCreator.deleteContact(id)),
    [dispatch]
  );

  const onDelete = (id) => {
    handleContactDelete(id);
    navigation.navigate('Contacts');
  };

  const onCall = (item) => {
    Communications.phonecall(item.phone, true);
  };

  const renderButton = () =>
    item.phone ? (
      <View style={styles.centerWrp}>
        <View style={styles.buttonWrapper}>
          <TouchableOpacity
            onPress={() => {
              onCall(item);
            }}
            style={styles.callBtn}
          >
            <Text style={styles.deleteBtnText}>Call</Text>
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() => {
              onDelete(item.id);
            }}
            style={styles.deleteBtn}
          >
            <Text style={styles.deleteBtnText}>Delete</Text>
          </TouchableOpacity>
        </View>
      </View>
    ) : (
      <View style={styles.centerWrp}>
        <View style={styles.buttonWrapper}>
          <TouchableOpacity
            onPress={() => {
              onDeleteContact(item);
            }}
            style={styles.deleteBtnSingle}
          >
            <Text style={styles.deleteBtnText}>Delete</Text>
          </TouchableOpacity>
        </View>
      </View>
    );

  return (
    <View style={styles.contactInfoWrapper}>
      <View style={styles.photoWrapper}>
        <Image
          style={styles.image}
          source={{
            uri: item.image,
          }}
        />
      </View>
      <View style={styles.textWrapper}>
        <Text style={styles.textLabel}>Name</Text>
        <Text style={styles.mainText} ellipsizeMode="tail" numberOfLines={2}>
          {item.name}
        </Text>
        <Text style={styles.textLabel}>Phone number</Text>
        <Text style={styles.mainText} ellipsizeMode="tail" numberOfLines={2}>
          {item.phone}
        </Text>
      </View>
      {renderButton()}
    </View>
  );
};

export default ContactInfo;
