import { StyleSheet } from 'react-native';

export const styles = StyleSheet.create({
  contactsWrapper: {
    flex: 1,
    justifyContent: 'center',
  },
  modalTitle: {
    textAlign: 'center',
    marginTop: 20,
    fontSize: 24,
    fontWeight: 'bold',
  },
  closeModalButtonWrapper: {
    margin: 20,
  },
  closeBtnWrapper: {
    margin: 70
  }
});
