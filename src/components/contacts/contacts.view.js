import React from 'react';
import { View, Text, Animated, Modal, Button } from 'react-native';
import { styles } from './styles';
import ContactList from './components/contact-list/contact-list';
import SearchInput from './components/search-input/search-input';
import AddContact from './components/add-contact/add-contact';
import AddForm from './components/add-form/add-form';
import * as Contacts from 'expo-contacts';
import { useSelector, useDispatch } from 'react-redux';
import { contactActionCreator } from '../../store/actions';

const dataMappers = (array) => {
  return array.map((item) => {
    let image, phone;
    if (!item.image) {
      image =
        'https://www.pngkey.com/png/detail/114-1149878_setting-user-avatar-in-specific-size-without-breaking.png';
    } else {
      image = item.image.uri;
    }

    if (!item.phoneNumbers) {
      phone = null;
    } else {
      phone = item.phoneNumbers[0].number;
    }

    return {
      id: item.id,
      name: item.name,
      phone: phone,
      image: image,
    };
  });
};

const ContactsView = ({ navigation }) => {
  const { contacts } = useSelector((state) => ({
    contacts: state.contact.contacts,
  }));
  const [modalWindow, setModalWindow] = React.useState(false);
  const [filterQuery, setFilterQuery] = React.useState('');

  const dispatch = useDispatch();

  const handleContactLoad = React.useCallback(
    (contacts) => dispatch(contactActionCreator.loadContact(contacts)),
    [dispatch]
  );

  const handleContactAdd = React.useCallback(
    (contact) => dispatch(contactActionCreator.addContact(contact)),
    [dispatch]
  );

  React.useEffect(() => {
    (async () => {
      const { status } = await Contacts.requestPermissionsAsync();
      if (status === 'granted') {
        const { data } = await Contacts.getContactsAsync();

        if (data.length > 0) {
          handleContactLoad(dataMappers(data));
        }
      }
    })();
  }, [handleContactLoad]);

  const onSearch = (searchText) => {
    setFilterQuery(searchText);
  };

  const onAddContact = (item) => {
    handleContactAdd({
      ...item,
      image:
        'https://www.pngkey.com/png/detail/114-1149878_setting-user-avatar-in-specific-size-without-breaking.png',
      id: Math.random(),
    });
  };

  const onItemClick = (item) => {
    navigation.navigate('ContactInfo', { item });
  };

  const getFilteredContacts = () =>
    contacts.filter((contact) => contact.name.includes(filterQuery));

  return (
    <View style={styles.contactsWrapper}>
      <Modal visible={modalWindow}>
        <View style={styles.contactsWrapper}>
          <Text style={styles.modalTitle}>Add Contact</Text>
          <View style={styles.closeModalButtonWrapper}>
            <AddForm
              onAddContact={onAddContact}
              closeModal={() => setModalWindow(false)}
            />
            <View style={styles.closeBtnWrapper}>
              <Button
                title="Close modal"
                onPress={() => setModalWindow(false)}
              />
            </View>
          </View>
        </View>
      </Modal>
      <SearchInput handleSearch={onSearch} />
      <ContactList contacts={getFilteredContacts()} onItemClick={onItemClick} />
      <AddContact onOpenModal={() => setModalWindow(true)} />
    </View>
  );
};

export default ContactsView;
