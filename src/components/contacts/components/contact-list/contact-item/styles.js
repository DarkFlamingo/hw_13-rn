import { StyleSheet } from 'react-native';

export const styles = StyleSheet.create({
  contactWrapper: {
    display: 'flex',
    flexDirection: 'row',
    padding: 13,
    marginHorizontal: 20,
    marginVertical: 5,
    borderWidth: 1,
    borderRadius: 20,
    borderColor: '#908F8F',
    backgroundColor: '#D9D9D9'
  },
  contactImage: {
    borderRadius: 30,
    height: 50,
    width: 50,
    marginRight: 20
  },
  nameText: {
    fontSize: 16,
  },
  phoneText: {
    fontSize: 12
  }
})