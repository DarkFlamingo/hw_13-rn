import React from 'react';
import { TouchableOpacity, Text, Image, View } from 'react-native';
import { styles } from './styles';

const ContactItem = ({ item, onItemClick }) => {
  const handleOnPress = () => {
    onItemClick(item);
  }

  return (
    <TouchableOpacity style={styles.contactWrapper} onPress={handleOnPress}>
      <Image
        style={styles.contactImage}
        source={{
          uri: item.image,
        }}
      />
      <View>
        <Text style={styles.nameText}>{item.name}</Text>
        <Text style={styles.phoneText}>{item.phone}</Text>
      </View>
    </TouchableOpacity>
  );
};

export default ContactItem;
