import React from 'react';
import { View, ScrollView, FlatList } from 'react-native';
import ContactItem from './contact-item/contact-item';

const ContactList = ({ contacts, onItemClick }) => {
  const renderItem = ({ item }) => {
    return <ContactItem item={item} onItemClick={onItemClick} />;
  };

  return (
    <FlatList
      data={contacts}
      keyExtractor={(item) => JSON.stringify(item.id)}
      renderItem={renderItem}
    />
  );
};

export default ContactList;
