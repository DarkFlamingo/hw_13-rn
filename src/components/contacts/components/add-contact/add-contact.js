import React from 'react';
import { Button, View } from 'react-native';
import { styles } from './styles';

const AddContact = ({ onOpenModal }) => {
  return (
    <View style={styles.addButtonWrapper}>
      <Button title="Add Contact" onPress={onOpenModal} />
    </View>
  );
};

export default AddContact;
