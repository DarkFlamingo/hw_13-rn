import React from 'react';
import { View, Button, TextInput } from 'react-native';
import { Formik } from 'formik';
import { styles } from './styles';

const AddForm = ({ onAddContact, closeModal }) => {
  const handleSubmit = (values, action) => {
    onAddContact(values);
    action.resetForm();
    closeModal();
  };

  return (
    <View style={styles.formWrapper}>
      <Formik
        initialValues={{
          name: '',
          phone: '',
        }}
        onSubmit={handleSubmit}
      >
        {(props) => (
          <View>
            <TextInput
              style={styles.inputTop}
              value={props.values.name}
              placeholder="Enter name"
              onChangeText={props.handleChange('name')}
            />
            <TextInput
              style={styles.inputBottom}
              value={props.values.phone}
              placeholder="Enter phone"
              onChangeText={props.handleChange('phone')}
            />
            <View style={styles.addBtn}>
              <Button title="Add" onPress={props.handleSubmit} />
            </View>
          </View>
        )}
      </Formik>
    </View>
  );
};

export default AddForm;
