import { StyleSheet } from 'react-native';

export const styles = StyleSheet.create({
  formWrapper: {
    margin: 30
  },
  inputTop: {
    color: 'black',
    marginBottom: 20,
    marginHorizontal: 20,
    paddingHorizontal: 15,
    paddingVertical: 10,
    backgroundColor: '#D3D3D3',
    fontSize: 16,
    borderRadius: 10,
  },
  inputBottom: {
    color: 'black',
    marginBottom: 20,
    marginHorizontal: 20,
    paddingHorizontal: 15,
    paddingVertical: 10,
    backgroundColor: '#D3D3D3',
    fontSize: 16,
    borderRadius: 10,
  },
  addBtn: {
    margin: 30
  }
});
