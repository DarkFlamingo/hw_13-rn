import React from 'react';
import { TextInput } from 'react-native';
import { styles } from './styles';

const SearchInput = ({ handleSearch }) => {
  const [searchText, setSearchText] = React.useState('');

  const handleChangeText = (text) => {
    setSearchText(text);
    handleSearch(text);
  };

  return (
    <TextInput
      style={styles.input}
      value={searchText}
      onChangeText={handleChangeText}
      placeholder={'Search contact'}
    />
  );
};

export default SearchInput;
