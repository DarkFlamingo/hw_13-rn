import { StyleSheet } from 'react-native';

export const styles = StyleSheet.create({
  input: {
    color: 'black',
    marginVertical: 15,
    marginHorizontal: 20,
    paddingHorizontal: 15,
    paddingVertical: 10,
    backgroundColor: '#D3D3D3',
    fontSize: 16,
    borderRadius: 10,
  }
})