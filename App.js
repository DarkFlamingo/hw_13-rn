import React, { useEffect } from 'react';
import { StyleSheet, View, Text } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import Contacts from './src/components/contacts/contacts.view';
import ContactInfo from './src/components/contact-info/contact-info.view';
import { Provider } from 'react-redux';
import store from './src/store/store';

const Stack = createStackNavigator();

const App = () => {
  return (
    <Provider store={store}>
      <NavigationContainer>
        <Stack.Navigator initialRouteName={'Contacts'}>
          <Stack.Screen name="Contacts" component={Contacts} />
          <Stack.Screen name="ContactInfo" component={ContactInfo} />
        </Stack.Navigator>
      </NavigationContainer>
    </Provider>
  );
};

export default App;
